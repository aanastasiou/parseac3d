""" Provides functions to read and render AC3D files into VPython.
 
 Requires pyparsing (http://pyparsing.wikispaces.com/) and VPython (www.vpython.org)

 Author: Athanasios Anastasiou July 2009 (Many thanks to Paul McGuire for the optimisations)

 AC3D FEATURES SUPPORTED IN THIS VERSION
 The parser reads the complete set of tags found in an AC3D file but the rendering part honors only the features and objects supported by VPython.
 Specific features of the AC3D file are not taken into account (i intend to add them to future releases). 
 For example, an AC3D file carries the complete position vector and rotation matrix (3x3) for every object in order to correctly place it 
  in its own frame of reference. However, Blender (www.blender.org) does not work in this way as it applies the position offsets and rotations directly 
  upon the object's vertices. Therefore, not handling the 'pos' and 'rot' fields will cause absolutely no problems _for models exported by Blender_ (!!!)

 EXPORTING YOUR 3D MODELS FROM BLENDER (www.blender.org):
 To export an AC3D file from Blender, you would first have to create the geometry and make sure that the object is subdivided into triangles!
 Blender does this automatically if you right click to select the object, set the editor into "Edit Mode" and then press Ctrl+T. 
 From that point on, all quads will be substituted by 2 triangles."""

from pyparsing import nums, hexnums, alphas, alphanums
from pyparsing import Optional, Word, Combine, Regex, Literal, Group, OneOrMore, Suppress, QuotedString, Forward
from pyparsing import countedArray

import visual
import sys
import os

def parseStr2Float(aSet):
    return float(aSet[0])

def parseStr2IntDec(aSet):
    return int(aSet[0])

def parseStr2IntHex(aSet):
    return int(aSet[0],16)

def getAC3DBNF():    
    """Returns the main AC3D parser"""
    #Definition of elementary types (float, int, string, etc).
    #A Floating Point Number with optional Scientific Notation
    floatValue = Regex(r"[-+]?[0-9]+(\.[0-9]+)?([eE][0-9]+)?")
    floatValue.setParseAction(parseStr2Float)
    
    #A triplet of floats
    float3Value = floatValue * 3
    
    #An integer expressed in either decimal or hexadecimal notation
    intLiteral = Regex(r"[-+]?[0-9]+").setParseAction(parseStr2IntDec)
    hexLiteral = Regex(r"0[xX][0-9a-fA-F]+").setParseAction(parseStr2IntHex)
    intValue = hexLiteral | intLiteral
    
    #A string of characters (plus additional allowed characters not included in alphanums)
    chars = Word(alphanums+"_"+"-"+".")
    
    #A quoted string
    stringValue = QuotedString('"')
    
    #Definition of the elementary types that make up an AC3D file
    #Header entry, also determining the version.
    headerEntry = Suppress("AC3D") + Word(hexnums)("fileVersion")

    #A single material definition
    materialEntry = Group(Suppress("MATERIAL") +
                        stringValue("matName") +
                        Suppress("rgb")   + float3Value("rgb") +
                        Suppress("amb")   + float3Value("amb") +
                        Suppress("emis")  + float3Value("emis") +
                        Suppress("spec")  + float3Value("spec") +
                        Suppress("shi")   + intValue("shi") +
                        Suppress("trans") + floatValue("trans"))
    
    #An array of material definitions
    materialEntries = OneOrMore(materialEntry)
    
    #Definition of more elementary types that are ultimately used to compose the OBJECT entry
    objectName = Suppress("name") + stringValue("name")
    objectData = (Suppress("data") + intValue("len") + chars("values"))("data")
    objectCrease = Suppress("crease") + floatValue("crease")
    objectTexture = Suppress("texture") + stringValue("texture")
    objectTexRep = Suppress("texrep") + floatValue("repX") + floatValue("repY")
    objectRot = Suppress("rot") + (float3Value + float3Value + float3Value)("rotMatrix")
    objectLoc = Suppress("loc") + float3Value("loc")
    objectURL = Suppress("url") + stringValue("url")
    objectNumVert=((Suppress("numvert")+intValue("num")) + OneOrMore(Group(float3Value))("data"))("vertex");    
    objectNumsurfSurf = Suppress("SURF") + intValue("surfType")
    objectNumsurfMat = Suppress("mat") + intValue("surfMat")
    objectNumsurfRefs=(Suppress("refs") + intValue("numVerts")) + OneOrMore(Group(intValue+floatValue+floatValue))("surfDef")       
    objectNumsurf=((Suppress("numsurf") + intValue("num")) + OneOrMore(Group(objectNumsurfSurf + objectNumsurfMat + objectNumsurfRefs))("data"))("surface")
    
    #Every object definition terminates with a required "kids int" that 
    # determines how many of the following OBJECTs should be added as 
    # children of the currently parsed OBJECT. A Forward declaration is 
    # required here to construct the element objectKids that is itself part 
    # of the OBJECT structure.
    objectEntry = Forward()
    objectKids = Suppress("kids") + countedArray(Group(objectEntry))("children")

    #Finally, the OBJECT structure is defined
    objectEntry << (Suppress("OBJECT") + chars("objectType") +
                       (Optional(objectName)("name") &
                        Optional(objectCrease)  &
                        Optional(objectData)    &
                        Optional(objectTexture) &
                        Optional(objectTexRep)  &
                        Optional(objectRot)     &
                        Optional(objectLoc)     &
                        Optional(objectURL)     &
                        Optional(objectNumVert) &
                        Optional(objectNumsurf)) +
                    objectKids)
    
    #The final definition of an AC3D file. It is composed of an obligatory 
    # HEADER, optionally a number of material definitions followed by OBJECT 
    # definitions.
    ac3dfile = (headerEntry("header") +
                Optional(materialEntries)("materials") +
                OneOrMore(Group(objectEntry))("sceneObjects"))
    return ac3dfile

def realiseObjectList(resSet, objectList, parentObject=None):
    """Renders the contents of an AC3D file using VPython.
        The function is used recursively so that the hierarchical nature of a 3D scene is preserved to some extent.
        (i.e. It is possible to obtain a reference to a child object and apply transformations just to that part of the model and its decendants)
        NOTE: This is a very simple function that was put together in haste, primarily to check if the AC3D parser worked. Therefore, the renderer is only trying to render just the form of the object
        rather than setup materials and load textures too.
        
        Accepts: 
            resSet      : A Resource Set that is basically what is returned from the parser object. This Resource Set describes a 3D scene in full (objects, materials, textures, etc)
            objectList  : The top level of the object tree where rendering should start. Children objects will be followed automatically.
            parentObject: The parent object to which decendant children will be attached to
        """
    resMatls = resSet.materials
    #This part renders an object using VPython's faces
    retObjects = []
    for anObject in objectList:
        if not parentObject is None:
            newObject = visual.frame(frame=parentObject)
        else:
            newObject = visual.frame()
        if anObject.objectType == "poly":
            objVertexData = anObject.vertex.data
            #construct the set of faces within newObject
            pos = []
            norms = []
            clr = []
            for aFace in anObject.surface.data:
                faceSurfaceDefn = aFace.surfDef
                faceSurfaceMatl = aFace.surfMat
                #Read the surface's type and create it accordingly 
                #Position data (For the time this must be TRIANGLES)
                pos.append(objVertexData[faceSurfaceDefn[0][0]])
                pos.append(objVertexData[faceSurfaceDefn[1][0]])
                pos.append(objVertexData[faceSurfaceDefn[2][0]])
                #Calculate the face normal...
                vct = visual.cross(visual.vector(objVertexData[faceSurfaceDefn[0][0]]) - 
                                      visual.vector(objVertexData[faceSurfaceDefn[1][0]]),
                                   visual.vector(objVertexData[faceSurfaceDefn[0][0]]) - 
                                      visual.vector(objVertexData[faceSurfaceDefn[2][0]]))
                #...and it's magnitude to normalise it
                vct_mag = visual.mag(vct)
                #Append the face normals to the list.
                norms.extend([vct/vct_mag,]*3)
                #Append the face colors to the list.
                clr.extend([resMatls[faceSurfaceMatl]["rgb"]]*3)                
                #Now check if this is a double sided surface and create one or 
                # more triangle with reverse winding and normal. For a triangle 
                # this can be done in a very fast way just by flipping two point 
                # and reversing the normal
                isDouble = bool(aFace.surfType & 0x02);
                if isDouble:
                    #Just filp any two of the three vertices...
                    pos.append(objVertexData[faceSurfaceDefn[0][0]])
                    pos.append(objVertexData[faceSurfaceDefn[2][0]])
                    pos.append(objVertexData[faceSurfaceDefn[1][0]])
                    #...and reverse the normal
                    vct = -vct
                    norms.extend([vct/vct_mag,]*3)
                    clr.extend([resMatls[faceSurfaceMatl]["rgb"]]*3)
            #Finally! All the object's faces are added to the parent frame and the 3D object is created.           
            newFace = visual.faces(frame=newObject,
                                    pos=pos,
                                    normal=norms,
                                    color=clr)

        #If this object is a "parent" to other objects then traverse the 'children' branch and add them recursively.
        #TODO: In all my testing so far and for small models, this recursion will not cause any problems. It can of course be alleviated 
        #with the addition of an OBJECT stack structure to get rid of recursion     
        if anObject.children[0]:
            realiseObjectList(resSet, anObject.children[0], newObject)
        retObjects.append(newObject)        
    return retObjects #This will ultimately return a list of top level objects


if __name__=="__main__":
    #An elementary demonstration of how to use the functions provided in this program:
    #Turning off shaders as they might not be working in some GFX cards
    visual.scene.enable_shaders = False
    #Obtain the grammar object
    ac3dParser = getAC3DBNF()
    theFile = sys.argv[1]
    if os.path.exists(theFile):
        #parse the file
        Q = ac3dParser.parseFile(theFile, parseAll = True)
        #render the file.
        G = realiseObjectList(Q,Q['sceneObjects'])

        #At this point all you get is a list of top level scene objects. Most commonly, a 3D model is "wrapped" around an object of type World.
        #By manipulating this top level object you can re-position or rotate the loaded .ac model. This can be done through VPython like this:
        #Rotate the model
        G[0].rotate(angle=visual.pi/4,axis=[0,1,0]);
        #Position the model somewhere else in a 3D scene
        sphereAtOrigin=visual.sphere(radius=0.25); #Just placing a sphere at the origin for a viewer to appreciate the coordinates change better
        G[0].pos=visual.vector(1.0,1.0,4.0);
    else:
        sys.stdout.write("File %s does not exist\n" % theFile)
